# ReinforcementLearning_Series1



## Getting started

In this repo, there are two folders of content related to reinforcement learning.

The code contents are rearranged from Udemy course - [Artificial Intelligence: Reinforcement Learning in Python](https://www.udemy.com/course/artificial-intelligence-reinforcement-learning-in-python/)

Folder "basics" discuss both prediction and control problems by adopting - 1. Tabular Solution Methods - Dynamic programming, Monte Carlo, Temporal-Difference learning, and Q learning. 2. Approximation Solution Methods - RBF combined with Q learning.

Folder "stockTradeProj" discuss one Approximation Method - Linear Regression model combined with Q learning on a stock trade problem.

