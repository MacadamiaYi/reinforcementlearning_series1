import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from datetime import datetime
import argparse
import re
import sys
import os
import pickle

from sklearn.preprocessing import StandardScaler

from Agent.agent import DQNAgent
from Env.stockTrade import MultiStockEnv


def get_data():
    df = pd.read_csv('./aapl_msi_sbux.csv')
    return df.values

def get_scaler(env):
    states = []
    for i in range(env.n_step):
        action = np.random.choice(env.action_space)
        next_state, reward, done, info = env.step(action)
        states.append(next_state)
        if done:
            break
    scaler = StandardScaler()
    scaler.fit(states)
    return scaler

def make_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def play_one_episode(agent, env, is_train):
    state = env.reset()
    # remember to scale the states
    s = scaler.transform([state])
    done = False
    while not done:
        a = agent.act(s)
        next_s, reward, done, info = env.step(a)
        # remember to scale the states
        next_s = scaler.transform([next_s])
        if is_train == 'train':
            agent.train(s,a,reward,next_s,done)
        s = next_s
    
    return info['cur_val']

if __name__ == '__main__':
    models_folder = 'linear_rl_trader_models'
    rewards_folder = 'linear_rl_trader_models'
    num_episodes = 2000
    batch_size = 32
    initial_investment = 20000

    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--mode', type = str, required=True, help = 'either "train" or "test"')
    
    args = parser.parse_args()

    make_dir(models_folder)
    make_dir(rewards_folder)
    
    data = get_data()
    n_timesteps, n_stocks = data.shape

    n_train = n_timesteps // 2

    train_data = data[:n_train]
    test_data = data[n_train:]

    env = MultiStockEnv(train_data, initial_investment)
    state_size = env.state_dim
    action_size = len(env.action_space)
    agent = DQNAgent(state_size, action_size)
    scaler = get_scaler(env)

    #STORE THE FINAL VALUE OF THE PORTFOLIO
    portfolio_value = []

    if args.mode == "test":
        # use the same scaler during training
        with open(f'{models_folder}/scaler.pkl', 'rb') as f:
            scaler = pickle.load(f)
        # use env for test data instead of train data above (override)
        env = MultiStockEnv(test_data, initial_investment)

        agent.epsilon = 0.01
        agent.load(f'{models_folder}/linear.npz')

    # play the game num_episodes times
    for e in range(num_episodes):
        t0 = datetime.now()
        val = play_one_episode(agent, env, args.mode)
        t1 = datetime.now()
        dt = t1 -t0
        print(f"episode:{e+1}/{num_episodes}, episode end value:{val:.2f}, duration:{dt}")
        portfolio_value.append(val)

    if args.mode == "train":
        agent.save(f'{models_folder}/linear.npz')
        with open(f'{models_folder}/scaler.pkl', 'wb') as f:
            pickle.dump(scaler, f)

        plt.plot(agent.model.losses)
        plt.show()

    np.save(f'{rewards_folder}/{args.mode}.npy', portfolio_value)

    plt.hist(portfolio_value)
    plt.title(args.mode)
    plt.show()

        

