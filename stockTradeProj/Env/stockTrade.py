
# states (n stock --> each state size: 2n+1)
# 1. how many shares of each stock I own
# 2. curent price of each stock
# 3. how much cash I have now

# actions (n stock --> each action size: n) combinations: 3^n (action space)
# buy/sell/hold for each stock 
# simplification: (1) no transaction cost 
# (2) if sell, sell all shares of stocks
# (3) if buy, buy as many shares as possible for the selected stock, how to select stocks?
#  can be considered as a knapsack problem, but here simplified as buy in loop, every time 1 share of each stock until no money
# (4) every time sell before buy

# reward
# change in value of portfolio from one step (state s) to the next (state s')
# portfolio value = s^{T}p + c
# s = vector of shares of each stock
# p = vector of per share price of each stock
# c = cash now in hand

import numpy as np
import itertools

class MultiStockEnv:
    # stock_prices provides the change of the value of each stock over time
    # initial_investment is the initial cash we have
    """
    State: vectors of size 7 (n_stock * 2 + 1)
    - # shares of stock 1 owned
    - # shares of stock 2 owned
    - # shares of stock 3 owned
    - price of stock 1 
    - price of stock 2
    - price of stock 3
    - cash owned

    Action: categorical variable with 27 (3^3) possibilities
    - for each stock:
    - 0 = sell
    - 1 = hold
    - 2 = buy 
    """
    def __init__(self, stock_prices, initial_investment = 20000):
        self.stock_price_history = stock_prices
        self.n_step, self.n_stock = self.stock_price_history.shape

        self.initial_investment = initial_investment
        self.cur_step = None    # tell the current date 
        self.stock_owned = None
        self.stock_price = None
        self.cash_in_hand = None

        self.action_space = np.arange(3**self.n_stock)
        # all combination list of [0,0,0], [0,0,1], ..., [2,2,2]
        self.action_list = list(map(list, itertools.product([0,1,2], repeat = 3)))
        self.state_dim = self.n_stock*2 + 1
        
        self.reset()

    def reset(self):
        # reset pointer to date 0 and return initial state
        self.cur_step = 0
        # own nothing at beginning
        self.stock_owned = np.zeros(self.n_stock)
        self.stock_price = self.stock_price_history[self.cur_step]
        self.cash_in_hand = self.initial_investment
        return self._get_obs()

    def step(self, action):
        # perform the trade, move the pointer
        # calculate the reward, next state, portfolio value, done
        assert action in self.action_space

        pre_val = self._get_val()
        self.cur_step += 1
        self.stock_price = self.stock_price_history[self.cur_step]
        self._trade(action)

        cur_val = self._get_val()
        reward = cur_val - pre_val
        done = self.cur_step == self.n_step -1 
        info = {'cur_val':cur_val}

        return self._get_obs(), reward, done, info
    
    def _get_obs(self):
        obs = np.zeros(self.state_dim)
        obs[:self.n_stock] = self.stock_owned
        obs[self.n_stock:2*self.n_stock] = self.stock_price
        obs[-1] = self.cash_in_hand
        
        return obs

    def _get_val(self):
        return self.stock_owned.dot(self.stock_price) + self.cash_in_hand
    
    def _trade(self, action):
        action_vec = self.action_list[action]

        sell_index = []
        buy_index = []
        for i, item in enumerate(action_vec):
            if item == 0:
                sell_index.append(i)
            if item == 2:
                buy_index.append(i)

        if sell_index:
            for i in sell_index:
                self.cash_in_hand += self.stock_price[i]*self.stock_owned[i]
                self.stock_owned[i] = 0

        if buy_index: 
            can_buy = True
            while can_buy:  # if buy always buying until run out of money
                for i in buy_index:
                    if self.cash_in_hand > self.stock_price[i]:
                        self.stock_owned[i] += 1
                        self.cash_in_hand -= self.stock_price[i]
                    else:
                        can_buy = False 

        




    


