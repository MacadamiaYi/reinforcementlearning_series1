# instead of modelling Q(s,a) with linear regression (use (s,a) as inputs, Q as outputs)
# we use only states s as inputs, and Q_a as outputs (the number of network outputs is the actionspace number |a|)
# instead of a scalar value as a target when modelling Q(s,a), which is just corresponds to the network output, now this approach network output is a vector of size |a|
# everytime when we perform an action "a" and get a new target (r+gamma*max_{a'}Q(s',a')), 
# only weights that corresonding to this action "a" in this network should be updated by cost J = (r+gamma*max_{a'}Q(s',a') - Q(s,a))^2
# thus, we have W_{a} = W_{a} = \eta * (dJ/dW_{a}) and b_{a} = b_{a} = \eta * (dJ/db_{a})
 

# Q(s) = W^{T}X + b, where W is with size (|s|, |a|), b is with size (|a|)
# target (label value): t = r if state is terminal, otherwise, t = r + gamma*max_{a'}Q(s',a')


# use gradient descent with momentum to update
# v(t) = \mu*v(t-1) - \eta*g(t)   (g(t) - gradient at time t, \mu - momentum term (0.9, 0.99, etc), \eta - learning rate)
# w(t) = w(t-1) + v(t) (w(t) is the updated weights W at time t)

import numpy as np

class linearModel:
    def __init__(self, input_dim, n_action):
        self.W = np.random.rand(input_dim, n_action) / np.sqrt(input_dim)
        self.b = np.zeros(n_action)

        # momentum terms
        self.vW = 0
        self.vb = 0

        self.losses = []

    def predict(self, X):
        # make sure X is N*|s| two dimensions (N is sample size, |s| is state size - 2n+1)
        assert(len(X.shape) == 2)
        return X.dot(self.W) + self.b
    
    def sgd(self, X, Y, learning_rate = 0.01, momentum = 0.9):
        assert(len(X.shape) == 2)
        # check the total number of values in Y ( N sample size * |a| actionspace size) 
        # in order to calculate the mean squared error
        num_values = np.prod(Y.shape)
        # J = (1/num_values)sum_{n}sum_{k}(y_{n,k} - \hat{y}_{n,k})^2 (n is sample index, k is action index)
        # dJ/dW = (2/num_values)sum_{n}sum_{k}(y_{n,k} - \hat{y}_{n,k})*(d y_{n,k}/dW)
        # dJ/db = (2/num_values)sum_{n}sum_{k}(y_{n,k} - \hat{y}_{n,k})*(d y_{n,k}/db)
        Yhat = self.predict(X)
        gW = 2*X.T.dot(Yhat-Y)/num_values   # since Y = WX+b, so (d y_{n,k}/dW) = X
        gb = 2*(Yhat-Y).sum(axis=0)/num_values   # since Y = WX+b, so (d y_{n,k}/db) = 1
        
        # update momentum terms
        self.vW = momentum*self.vW - learning_rate*gW
        self.vb = momentum*self.vb - learning_rate*gb

        self.W += self.vW
        self.b += self.vb

        mse = np.mean((Yhat-Y)**2)
        self.losses.append(mse)

    def load_weights(self, filepath):
        m = np.load(filepath)
        self.W = m['W']
        self.b = m['b']

    def save_weights(self, filepath):
        np.savez(filepath, W=self.W, b=self.b)

