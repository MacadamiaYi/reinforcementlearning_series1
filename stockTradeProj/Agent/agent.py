
import sys
import numpy as np
import os
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(CURRENT_DIR)
from pathConfig import Model_FOLDER
sys.path.append(Model_FOLDER)
from linearRegressionModel import linearModel

class DQNAgent(object):
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.gamma = 0.95 # discount rate
        self.epsilon = 1.0 
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.model = linearModel(state_size, action_size)

    def act(self, state): # epsilon greedy
        # calculate Q(s,a), take the argmax over a
        if np.random.rand() <= self.epsilon:
            return np.random.choice(self.action_size)
        else:
           val = self.model.predict(state) # output is (sample size * action space size)
           return np.argmax(val[0])

    def train(self, state, action, reward, next_state, done):
        if done:
            target = reward
        else:
            # axis = 1 means maximum along the second axis (column) (namely, action space)
            # select the column who has the largest element
            target = reward + self.gamma * np.amax(self.model.predict(next_state), axis = 1) 

        # everytime when we perform an action "a" and get a new target (r+gamma*max_{a'}Q(s',a')), 
        # only weights that corresonding to this action "a" in this network should be updated by cost J = (r+gamma*max_{a'}Q(s',a') - Q(s,a))^2
        target_full = self.model.predict(state)
        target_full[0, action] = target 

        # update the gradients
        self.model.sgd(state, target_full)

        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
    
    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)

