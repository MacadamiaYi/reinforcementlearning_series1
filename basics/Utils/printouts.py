def print_values(V, g):
    for i in range(g.rows):
        print("--------------------")
        for j in range(g.cols):
            v = V.get((i, j), 0)
            if v >= 0:
                print(" %.2f|"% v, end = "")
            else:
                print("%.2f|"% v, end = "")
        print("")

def print_policy(policy, g):
    for i in range(g.rows):
        print("--------------------")
        for j in range(g.cols):
            a = policy.get((i,j), '-')
            print(" %s |"% a, end = "")
        print("")

def get_transition_probs_rewards(grid):
    transition_prob = {}
    rewards = {}
    for (s1, a), s2p in grid.probs.items():
        for s2, p in s2p.items():
            transition_prob[(s1, a, s2)] = p
            rewards[(s1, a, s2)] = grid.rewards.get(s2, 0)
    
    return transition_prob, rewards