import sys
from path_config import Env_FOLDER, Util_FOLDER
sys.path.append(Env_FOLDER)
from gridWorld import ACTION_SPACE, standard_grid
sys.path.append(Util_FOLDER)
from printouts import print_values, print_policy, get_transition_probs_rewards
import numpy as np
import matplotlib.pyplot as plt
from sklearn.kernel_approximation import Nystroem, RBFSampler

EPSILON = 1e-3
ALPHA = 0.1
gamma = 0.9

def epsilon_greedy(grid, policy, s, EPSILON = 0.1):
    rnd = np.random.random()
    if rnd < (1-EPSILON):
        a = policy[s]
    else:
        a = np.random.choice(grid.actions[s])

    return a

def gather_samples(grid, n_episodes = 10000):
    samples = []
    s = grid.reset()
    samples.append(s)
    while not grid.game_over():
        a = np.random.choice(ACTION_SPACE)
        r = grid.move(a)
        s = grid.current_state()
        samples.append(s)

    return samples

class Model:
    def __init__(self, grid) -> None:
        samples = gather_samples(grid)
        #self.featurizer = Nystroem()
        self.featurizer = RBFSampler()
        self.featurizer.fit(samples)
        #dims = self.featurizer.components_.shape[0]
        dims = self.featurizer.random_offset_.shape[0]

        self.w = np.zeros(dims)

    def predict(self, s):
        x = self.featurizer.transform([s])[0] # gather new sample
        return x@self.w  # dot product
    
    def grad(self, s):
        x = self.featurizer.transform([s])[0]
        return x

if __name__ == "__main__":
    
    grid = standard_grid()
    print("rewards:")
    print_values(grid.rewards, grid)
    
    # initialize a policy
    greedy_policy = {
        (2,0):'U',
        (1,0):'U',
        (0,0):'R',
        (0,1):'R',
        (0,2):'R',
        (1,2):'R',
        (2,1):'R',
        (2,2):'R',
        (2,3):'U' }
    
    model = Model(grid)

    Iteration = 20000
    mse_per_episode = []
    for i in range(Iteration):
        if i % 200 == 0:
            print(i)
        step = 0
        total_episode_err = 0
        s = grid.reset()
        while not grid.game_over():
            a = epsilon_greedy(grid, greedy_policy, s)
            r = grid.move(a)
            next_s = grid.current_state()
            new_goal = 0
            if next_s not in grid.actions: # final state
                new_goal = r
            else:
                Vs_next = model.predict(next_s)
                new_goal = r + gamma*Vs_next

            # w = w + learning rate * (new_goal - Vs) * x
            x = model.grad(s)
            Vs = model.predict(s)
            # update the model
            model.w += ALPHA * (new_goal - Vs) * x

            err = new_goal - Vs
            step += 1
            total_episode_err += err*err
            s = next_s

        mse = total_episode_err/step
        mse_per_episode.append(mse)

    plt.plot(mse_per_episode)
    plt.show()

    V = {}
    for s in grid.all_states():
        if s in grid.actions:
            V[s] = model.predict(s)
        else:
            V[s] = 0

    print("final Values from policy iteration:")
    print_values(V, grid)

    print("initial policy:")
    print_policy(greedy_policy, grid)
 


    
