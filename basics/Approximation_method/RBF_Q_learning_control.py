import sys
from path_config import Env_FOLDER, Util_FOLDER
sys.path.append(Env_FOLDER)
from gridWorld import ACTION_SPACE, standard_grid
sys.path.append(Util_FOLDER)
from printouts import print_values, print_policy, get_transition_probs_rewards
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.kernel_approximation import Nystroem, RBFSampler

EPSILON = 1e-3
ALPHA = 0.1
gamma = 0.9
ACTION2INT = {a: i for i, a in enumerate(ACTION_SPACE)}
INT2ONEHOT = np.eye(len(ACTION_SPACE))

def epsilon_greedy(model, s, EPSILON = 0.1):
    rnd = np.random.random()
    if rnd < (1-EPSILON):
        values = model.predict_all_actions(s)
        return ACTION_SPACE[np.argmax(values)]
    else:
        return np.random.choice(ACTION_SPACE)

def one_hot(k):
    return INT2ONEHOT[k]

def merge_state_action(s,a):
    ai = one_hot(ACTION2INT[a])
    return np.concatenate((s, ai))

def gather_samples(grid, n_episodes = 10000):
    samples = []
    s = grid.reset()
    while not grid.game_over():
        a = np.random.choice(ACTION_SPACE)
        sa = merge_state_action(s,a)
        samples.append(sa)
        r = grid.move(a)
        s = grid.current_state()

    return samples

class Model:
    def __init__(self, grid) -> None:
        samples = gather_samples(grid)
        #self.featurizer = Nystroem()
        self.featurizer = RBFSampler()
        self.featurizer.fit(samples)
        #dims = self.featurizer.components_.shape[0]
        dims = self.featurizer.random_offset_.shape[0]

        self.w = np.zeros(dims)

    def predict(self, s, a):
        sa = merge_state_action(s,a)
        x = self.featurizer.transform([sa])[0] # gather new sample
        return x@self.w  # dot product
    
    def predict_all_actions(self, s):
        return [self.predict(s,a) for a in ACTION_SPACE]
    
    def grad(self, s, a):
        sa = merge_state_action(s,a)
        x = self.featurizer.transform([sa])[0]
        return x
    
# as there is no argmax function for dictionary, thus, we need this function
def max_dict(q_s):

    max_val = max(q_s.values())
    max_actions = [key for key, val in q_s.items() if val == max_val]

    return np.random.choice(max_actions), max_val


if __name__ == "__main__":
    
    grid = standard_grid(stepReward= -0.2)
    print("rewards:")
    print_values(grid.rewards, grid)
    
    model = Model(grid)
    Iteration = 20000
    # keep track of how many times Q[s] has been updated
    state_visit_counts = {}
    reward_per_episode = []

    for i in range(Iteration):
        if i % 2000 == 0:
            print("it:", i)

        episode_reward = 0

        s = grid.reset()
        state_visit_counts[s] = state_visit_counts.get(s,0) + 1
        while not grid.game_over():
            a = epsilon_greedy(model, s)
            r = grid.move(a)
            episode_reward += r
            next_s = grid.current_state()
            new_goal = 0
            if grid.game_over():
                new_goal = r
            else:
                max_q = np.max(model.predict_all_actions(next_s))
                new_goal = r + gamma*max_q

            old_q = model.predict(s,a)
            x = model.grad(s, a)
            # w = w + learning rate * (new_goal - Q[s][a]) * x
            model.w += ALPHA*(new_goal - old_q)*x

            state_visit_counts[next_s] = state_visit_counts.get(next_s,0) + 1
            s = next_s

        reward_per_episode.append(episode_reward)

    #plt.plot(delta)
    plt.plot(reward_per_episode)
    plt.show()

    policy = {}
    V = {}
    for s in grid.all_states():
        if s in grid.actions:
            values = model.predict_all_actions(s)
            V[s] = np.max(values)
            policy[s] = ACTION_SPACE[np.argmax(values)]
        else:
            V[s] = 0

    print("values:")
    print_values(V, grid)

    print("final policy:")
    print_policy(policy, grid)

    print("state visit counts:")
    state_sample_cnt = np.zeros((grid.rows, grid.cols))
    
    for i in range(grid.rows):
        for j in range(grid.cols):
            if (i,j) in state_visit_counts:
                state_sample_cnt[(i,j)] = state_visit_counts[(i,j)]

    df = pd.DataFrame(state_sample_cnt)
    print(df)


    
 


    
