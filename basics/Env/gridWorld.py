import numpy as np
import random

ACTION_SPACE = ['U', 'D', 'L', 'R']

class grid_world:
    def __init__(self, row, column, start =(2,0)):
        self.rows = row
        self.cols = column
        self.i = start[0]
        self.j = start[1]

    def set(self, rewards, actions, probs):
        self.rewards = rewards  # {(i,j): r} 
        self.actions = actions  # {(i,j): A}  A is a list of possible actions
        self.probs = probs      # {((i,j), a): {(m1,n1): prob1, (m2,n2),prob2} }

    def is_terminal(self, s):
        return s not in self.actions
    
    def move(self, action):
        if action in self.actions[(self.i, self.j)]:
            if action == 'U':
                self.i -= 1
            if action == 'D':
                self.i += 1
            if action == 'L':
                self.j -= 1
            if action == 'R':
                self.j += 1

        return self.rewards.get((self.i, self.j), 0)
    
    def all_states(self):
        return self.actions.keys() | self.rewards.keys()
    
    def set_states(self, s):
        self.i = s[0]
        self.j = s[1]
    
    def current_state(self):
        return (self.i, self.j)
    
    def game_over(self):
        return (self.i, self.j) not in self.actions

    def reset(self):
        start =(2,0)
        self.i = start[0]
        self.j = start[1]
        return start


class stochastic_world(grid_world):
    def __init__(self, row, column, start):
        super().__init__(row, column, start)

    def move(self, action):
        next_states = self.probs[((self.i, self.j),action)].keys()
        probability = self.probs[((self.i, self.j),action)].values()
        #print([list(next_states), list(probability)])
        next_state = random.choices(list(next_states), weights = list(probability), k = 1)[0]

        self.i = next_state[0]
        self.j = next_state[1]
        return self.rewards.get((self.i, self.j), 0)


#   |*| *  |*|E(+1)Win |
#   |*|wall|*|E(-1)Lose|
#   |S| *  |*|*        |
def standard_grid(stepReward = 0):
    rows = 3
    columns = 4
    start = (2, 0)
    g = grid_world(rows, columns, start)

    rewards = {
        (0,0):stepReward,
        (0,1):stepReward,
        (0,2):stepReward,
        (0,3):1,
        (1,0):stepReward,
        (1,2):stepReward,
        (1,3):-1,
        (2,0):stepReward,
        (2,1):stepReward,
        (2,2):stepReward,
        (2,3):stepReward
    }

    actions = {
        (0,0):('D', 'R'),
        (0,1):('R', 'L'),
        (0,2):('D', 'R', 'L'),
        (1,0):('U', 'D'),
        (1,2):('U', 'D', 'R'),
        (2,0):('U', 'R'),
        (2,1):('L', 'R'),
        (2,2):('U', 'L', 'R'),
        (2,3):('U', 'L')
    }
    # {((i,j), a): {(m1,n1): prob1, (m2,n2),prob2} }
    probs = {
        ((0,0), 'D'): {(1,0): 1},
        ((0,0), 'R'): {(0,1): 1},
        ((0,1), 'R'): {(0,2): 1},
        ((0,1), 'L'): {(0,0): 1},
        ((0,2), 'D'): {(1,2): 1},
        ((0,2), 'L'): {(0,1): 1},
        ((0,2), 'R'): {(0,3): 1},
        ((1,0), 'U'): {(0,0): 1},
        ((1,0), 'D'): {(2,0): 1},
        ((1,2), 'U'): {(0,2): 1}, #(0,2): 0.7, (1,3):0.3
        ((1,2), 'D'): {(2,2): 1},
        ((1,2), 'R'): {(1,3): 1},
        ((2,0), 'U'): {(1,0): 1},
        ((2,0), 'R'): {(2,1): 1},
        ((2,1), 'R'): {(2,2): 1},
        ((2,1), 'L'): {(2,0): 1},
        ((2,2), 'U'): {(1,2): 1},
        ((2,2), 'L'): {(2,1): 1},
        ((2,2), 'R'): {(2,3): 1},
        ((2,3), 'U'): {(1,3): 1},
        ((2,3), 'L'): {(2,2): 1}
    }

    g.set(rewards, actions, probs)
    
    return g
    
#   |*| *  |*|E(+1)Win |
#   |*|wall|*|E(-1)Lose|
#   |S| *  |*|*        |
def stochastic_grid(stepReward = 0):
    rows = 3
    columns = 4
    start = (2, 0)
    g = stochastic_world(rows, columns, start)

    rewards = {
        (0,0):stepReward,
        (0,1):stepReward,
        (0,2):stepReward,
        (0,3):1,
        (1,0):stepReward,
        (1,2):stepReward,
        (1,3):-1,
        (2,0):stepReward,
        (2,1):stepReward,
        (2,2):stepReward,
        (2,3):stepReward
    }

    actions = {
        (0,0):('D', 'R'),
        (0,1):('R', 'L'),
        (0,2):('D', 'R', 'L'),
        (1,0):('U', 'D'),
        (1,2):('U', 'D', 'R'),
        (2,0):('U', 'R'),
        (2,1):('L', 'R'),
        (2,2):('U', 'L', 'R'),
        (2,3):('U', 'L')
    }
    # {((i,j), a): {(m1,n1): prob1, (m2,n2),prob2} }
    probs = {
        ((0,0), 'D'): {(1,0): 1},
        ((0,0), 'R'): {(0,1): 1},
        ((0,1), 'R'): {(0,2): 1},
        ((0,1), 'L'): {(0,0): 1},
        ((0,2), 'D'): {(1,2): 1},
        ((0,2), 'L'): {(0,1): 1},
        ((0,2), 'R'): {(0,3): 1},
        ((1,0), 'U'): {(0,0): 1},
        ((1,0), 'D'): {(2,0): 1},
        ((1,2), 'U'): {(0,2): 0.5, (1,3):0.5},
        ((1,2), 'D'): {(2,2): 1},
        ((1,2), 'R'): {(1,3): 1},
        ((2,0), 'U'): {(1,0): 1},
        ((2,0), 'R'): {(2,1): 1},
        ((2,1), 'R'): {(2,2): 1},
        ((2,1), 'L'): {(2,0): 1},
        ((2,2), 'U'): {(1,2): 1},
        ((2,2), 'L'): {(2,1): 1},
        ((2,2), 'R'): {(2,3): 1},
        ((2,3), 'U'): {(1,3): 1},
        ((2,3), 'L'): {(2,2): 1}
    }

    g.set(rewards, actions, probs)
    
    return g
    