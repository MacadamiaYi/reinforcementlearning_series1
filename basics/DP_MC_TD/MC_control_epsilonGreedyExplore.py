import sys
from path_config import Env_FOLDER, Util_FOLDER
sys.path.append(Env_FOLDER)
from gridWorld import ACTION_SPACE, standard_grid
sys.path.append(Util_FOLDER)
from printouts import print_values, print_policy, get_transition_probs_rewards
import numpy as np
import matplotlib.pyplot as plt

EPSILON = 1e-3
gamma = 0.9

def epsilon_greedy(grid, policy, s, epsilon = 0.1):

    p = np.random.random()

    if p > epsilon:
        action = policy[s]
    else:
        action = np.random.choice(grid.actions[s])

    return action

def play_game(grid, policy, max_steps = 20):

    s = grid.reset()  # every time reset to an initial state
    a = epsilon_greedy(grid, policy, s)

    states = [s]
    actions = [a]
    rewards = [0]  # to make index consistent with states
    
    step = 0
    while not grid.game_over():
        r = grid.move(a)
        s = grid.current_state()

        if not grid.game_over():
            a = epsilon_greedy(grid, policy, s)

        rewards.append(r)
        states.append(s)
        actions.append(a)

        step += 1
        if step >= max_steps:
            break
        
    return states, actions, rewards

# as there is no argmax function for dictionary, thus, we need this function
def max_dict(q_s):

    max_val = max(q_s.values())
    max_actions = [key for key, val in q_s.items() if val == max_val]

    return np.random.choice(max_actions), max_val


if __name__ == "__main__":
    
    grid = standard_grid()
    transition_prob, rewards = get_transition_probs_rewards(grid)

    print("rewards:")
    print_values(grid.rewards, grid)
    
    # randomly generate a policy
    policy = {}
    for s in grid.all_states():
        if s in grid.actions:
            a = np.random.choice(grid.actions[s])
            policy[s] = a
    
    
    Q = {}
    times = {}
    for s in grid.all_states():
        if s in grid.actions:
            Q[s] = {}
            times[s] = {}
            for a in grid.actions[s]:
                Q[s][a] = 0
                times[s][a] = 0

    Iteration = 9000
    delta = []
    for i in range(Iteration):
        states, actions, rewards = play_game(grid, policy) 
        T = len(states) # T-1 is the last terminal state with V = 0 (no need), thus, start from T-2
        G = 0
        old_Q = 0
        gap = float('-inf')
        for idx in range(T-2, -1, -1):
            # G_{s}(t) = r(t+1) + gamma*G_{s'}(t+1)
            s = states[idx]
            a = actions[idx]
            r = rewards[idx+1]
            G = r + gamma*G
            if s not in states[:idx]:
                times[s][a] += 1
                old_Q = Q[s][a]
                Q[s][a] = old_Q + (G - old_Q) /times[s][a]

                policy[s] = max_dict(Q[s])[0]
                gap = max(gap, abs(old_Q - Q[s][a]))
        delta.append(gap)

    plt.plot(delta)
    plt.show()

    print("final policy:")
    print_policy(policy, grid)

    V = {}
    for s in grid.all_states():
        if s in grid.actions:
            V[s] = max_dict(Q[s])[1]

    print("final Values from policy iteration:")
    print_values(V, grid)

    
 


    
