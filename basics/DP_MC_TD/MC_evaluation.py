import sys
from path_config import Env_FOLDER, Util_FOLDER
sys.path.append(Env_FOLDER)
from gridWorld import ACTION_SPACE, stochastic_grid
sys.path.append(Util_FOLDER)
from printouts import print_values, print_policy, get_transition_probs_rewards
import numpy as np

EPSILON = 1e-3
gamma = 0.9

def play_game(grid, policy, max_steps = 20):

    entire_states = list(grid.actions.keys())
    start_state_index = np.random.choice(len(entire_states))
    start_state = entire_states[start_state_index]
    grid.set_states(start_state)      # every time have a random start to enable exploring

    states = [start_state]
    rewards = [0]  # to make index consistent with states
    s = start_state
    step = 0
    while not grid.game_over():
        action = policy[s]
        rewards.append(grid.move(action))
        s = grid.current_state()
        states.append(s)

        step += 1
        if step >= max_steps:
            break
        
    return states, rewards


if __name__ == "__main__":
    
    grid = stochastic_grid()
    transition_prob, rewards = get_transition_probs_rewards(grid)

    print("rewards:")
    print_values(grid.rewards, grid)
    
    # initialize a policy
    policy = {
        (2,0):'U',
        (1,0):'U',
        (0,0):'R',
        (0,1):'R',
        (0,2):'R',
        (1,2):'R',
        (2,1):'R',
        (2,2):'R',
        (2,3):'U' }
    
    
    V = {}
    return_val = {}
    for s in grid.all_states():
        if s in grid.actions:
            return_val[s] = []
        else:
            V[s] = 0

    Iteration = 1000
    
    for i in range(Iteration):
        states, rewards = play_game(grid, policy) 
        T = len(states) # T-1 is the last terminal state with V = 0 (no need), thus, start from T-2
        G = 0
        for idx in range(T-2, -1, -1):
            # G_{s}(t) = r(t+1) + gamma*G_{s'}(t+1)
            s = states[idx]
            r = rewards[idx+1]
            G = gamma*G + r     
            if s not in states[:idx]: # first-visit MC
                return_val[s].append(G)
                V[s] = np.mean(return_val[s])


    print("final Values from policy iteration:")
    print_values(V, grid)

    print("initial policy:")
    print_policy(policy, grid)
 


    
