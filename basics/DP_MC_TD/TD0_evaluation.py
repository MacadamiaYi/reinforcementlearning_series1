import sys
from path_config import Env_FOLDER, Util_FOLDER
sys.path.append(Env_FOLDER)
from gridWorld import ACTION_SPACE, standard_grid
sys.path.append(Util_FOLDER)
from printouts import print_values, print_policy, get_transition_probs_rewards
import numpy as np
import matplotlib.pyplot as plt

EPSILON = 1e-3
ALPHA = 0.1
gamma = 0.9

def epsilon_greedy(grid, policy, s, EPSILON = 0.1):
    rnd = np.random.random()
    if rnd < (1-EPSILON):
        a = policy[s]
    else:
        a = np.random.choice(grid.actions[s])

    return a

if __name__ == "__main__":
    
    grid = standard_grid()
    print("rewards:")
    print_values(grid.rewards, grid)
    
    # initialize a policy
    policy = {
        (2,0):'U',
        (1,0):'U',
        (0,0):'R',
        (0,1):'R',
        (0,2):'R',
        (1,2):'R',
        (2,1):'R',
        (2,2):'R',
        (2,3):'U' }
    
    
    V = {}
    return_val = {}
    for s in grid.all_states():
        V[s] = 0

    Iteration = 10000
    deltas = []
    for i in range(Iteration):
        s = grid.reset()
        delta = float('-inf')
        while not grid.game_over():
            a = epsilon_greedy(grid, policy, s)
            r = grid.move(a)
            next_s = grid.current_state()

            old_v = V[s]
            V[s] = old_v + ALPHA*(r + gamma*V[next_s] - old_v)
            delta = max(delta, abs(V[s] - old_v))

            s = next_s

        deltas.append(delta)

    plt.plot(deltas)
    plt.show()

    print("final Values from policy iteration:")
    print_values(V, grid)

    print("initial policy:")
    print_policy(policy, grid)
 


    
