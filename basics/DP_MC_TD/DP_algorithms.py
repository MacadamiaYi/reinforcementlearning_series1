import sys
from path_config import Env_FOLDER, Util_FOLDER
sys.path.append(Env_FOLDER)
from gridWorld import ACTION_SPACE, stochastic_grid
sys.path.append(Util_FOLDER)
from printouts import print_values, print_policy, get_transition_probs_rewards
import numpy as np

EPSILON = 1e-3

def policy_evaluation(policy, grid, rewards, transition_prob, gamma):
    V = {}
    for s in grid.all_states():
        V[s] = 0
    idx = 0
    while True:
        max_val = 0
        old_V ={key: value for key, value in V.items()}
        for s1 in grid.all_states():
            new_v = 0 # every time visit s1 again needs to start from 0/other initialization, not from V
            if not grid.is_terminal(s1):
                for a in ACTION_SPACE:
                    for s2 in grid.all_states():
                        #print([s1, a, s2])
                        # pi_prob = policy[s1].get(a, 0) for stochastic policy 
                        pi_prob = 1 if a == policy[s1] else 0 # for deterministic policy
                        env_prob = transition_prob.get((s1,a,s2),0)
                        r = rewards.get((s1, a, s2), 0)
                        new_v += pi_prob*env_prob*(r + gamma*old_V[s2]) 
            V[s1] = new_v
            max_val = max(max_val, abs(V[s1] - old_V[s1]))
        print("iteration: ", idx, " gap: ", max_val)
        print_values(V, grid)
        idx += 1
        if max_val < EPSILON:
            break

    return V

def policy_improvement(V, policy, grid, rewards, transition_prob, gamma):
    
    is_converged_stable = True

    for s1 in grid.actions.keys():
        best_value = float('-inf')
        old_a = policy[s1]
        new_a = None
        for a in ACTION_SPACE:
            new_v = 0 # this targets on each action, shall need to be initialized for every action
            for s2 in grid.all_states():
                r = rewards.get((s1,a, s2), 0)
                new_v += transition_prob.get((s1,a,s2),0)*(r + gamma*V[s2])

            if best_value < new_v:
                best_value = new_v
                new_a = a

        policy[s1] = new_a
        if old_a != new_a:
            is_converged_stable = False

    print('policy:')
    print_policy(policy, grid)
            
    return policy, is_converged_stable


def policy_iteration(policy, grid, rewards, transition_prob):
    while True:
        gamma = 0.9 # discount factor
        V = policy_evaluation(policy, grid, rewards, transition_prob, gamma)
        
        policy, is_converged_stable = policy_improvement(V, policy, grid, rewards, transition_prob, gamma)

        if is_converged_stable == True:
            break

    return policy, V


def value_iteration(policy, grid, rewards, transition_prob):
    gamma = 0.9 # discount factor
    V = {}
    for s in grid.all_states():
        V[s] = 0
    idx = 0
    while True:
        max_val = 0
        for s1 in grid.all_states():
            if not grid.is_terminal(s1):
                old_v = V[s1]
                new_v = float("-inf")
                for a in ACTION_SPACE:
                    v = 0  # summation under only different next-states
                    # not like in policy evaluation, summation under all actions and next states
                    for s2 in grid.all_states():
                        #no pi_prob, as we are trying to find max of action, instead of summation of all actions
                        env_prob = transition_prob.get((s1,a,s2),0)
                        r = rewards.get((s1, a, s2), 0)
                        v += env_prob*(r + gamma*V[s2]) 
                    if new_v < v:
                        new_v = v
                V[s1] = new_v
                max_val = max(max_val, abs(V[s1] - old_v))
        print("iteration: ", idx, " gap: ", max_val)
        print_values(V, grid)
        idx += 1
        if max_val < EPSILON:
            break

    #obtain the final policy
    for s1 in grid.actions.keys():
        best_value = float('-inf')
        new_a = None
        for a in ACTION_SPACE:
            new_v = 0 # this targets on each action, shall need to be initialized for every action
            for s2 in grid.all_states():
                r = rewards.get((s1,a, s2), 0)
                new_v += transition_prob.get((s1,a,s2),0)*(r + gamma*V[s2])

            if best_value < new_v:
                best_value = new_v
                new_a = a

        policy[s1] = new_a

    return policy, V

if __name__ == "__main__":
    
    grid = stochastic_grid()
    transition_prob, rewards = get_transition_probs_rewards(grid)

    print("rewards:")
    print_values(grid.rewards, grid)
    """
    # initialize a stochastic policy
    policy = {
        (0,0):{'R':1.0},
        (0,1):{'R':1.0},
        (0,2):{'R':1.0},
        (1,0):{'U': 1.0},
        (1,2):{'U':1.0},
        (2,0):{'U':0.5, 'R':0.5}, # start state is stochastic
        (2,1):{'R':1.0},
        (2,2):{'U':1.0},
        (2,3):{'L':1.0} }
    """
    # randomly generate a policy
    policy = {}
    for s in grid.actions.keys():
        policy[s] = np.random.choice(ACTION_SPACE)

    print("initial policy:")
    print_policy(policy, grid)
    
    optimal_policy, optimal_value = policy_iteration(policy, grid, rewards, transition_prob)

    print("final Values from policy iteration:")
    print_values(optimal_value, grid)

    print("final policy from policy iteration:")
    print_policy(optimal_policy, grid)


    # randomly generate a policy
    policy = {}
    for s in grid.actions.keys():
        policy[s] = np.random.choice(ACTION_SPACE)

    optimal_policy, optimal_value = value_iteration(policy, grid, rewards, transition_prob)

    print("final Values from value iteration:")
    print_values(optimal_value, grid)

    print("final policy from value iteration:")
    print_policy(optimal_policy, grid)




    
