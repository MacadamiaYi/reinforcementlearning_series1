import os

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_DIR = os.path.dirname(CURRENT_DIR)
Env_FOLDER = os.path.join(PROJECT_DIR, 'Env')
Util_FOLDER = os.path.join(PROJECT_DIR, 'Utils')