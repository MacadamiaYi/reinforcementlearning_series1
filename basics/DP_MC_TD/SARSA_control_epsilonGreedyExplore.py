import sys
from path_config import Env_FOLDER, Util_FOLDER
sys.path.append(Env_FOLDER)
from gridWorld import ACTION_SPACE, standard_grid
sys.path.append(Util_FOLDER)
from printouts import print_values, print_policy, get_transition_probs_rewards
import numpy as np
import matplotlib.pyplot as plt

EPSILON = 1e-3
ALPHA = 0.1
gamma = 0.9

def epsilon_greedy(Q, s, epsilon = 0.1):

    p = np.random.random()

    if p > epsilon:
        action = max_dict(Q[s])[0]
    else:
        action = np.random.choice(ACTION_SPACE)

    return action


# as there is no argmax function for dictionary, thus, we need this function
def max_dict(q_s):

    max_val = max(q_s.values())
    max_actions = [key for key, val in q_s.items() if val == max_val]

    return np.random.choice(max_actions), max_val


if __name__ == "__main__":
    
    grid = standard_grid(stepReward= -0.1)
    print("rewards:")
    print_values(grid.rewards, grid)
    
    
    Q = {}
    for s in grid.all_states():
        Q[s] = {}
        for a in ACTION_SPACE:
                Q[s][a] = 0

    Iteration = 10000
    # keep track of how many times Q[s] has been updated
    update_counts = {}
    deltas = []
    reward_per_episode = []

    for i in range(Iteration):
        if i % 2000 == 0:
            print("it:", i)

        delta = float('-inf')
        episode_reward = 0

        s = grid.reset()
        a = epsilon_greedy(Q, s)
        while not grid.game_over():
            r = grid.move(a)
            episode_reward += r
            next_s = grid.current_state()
            next_a = epsilon_greedy(Q, next_s)

            old_q = Q[s][a]
            Q[s][a] = Q[s][a] + ALPHA*(r + gamma*Q[next_s][next_a] - Q[s][a])

            delta = max(delta, abs(Q[s][a] - old_q))
            update_counts[s] = update_counts.get(s,0) + 1

            s = next_s
            a = next_a
        deltas.append(delta)
        reward_per_episode.append(episode_reward)

    #plt.plot(delta)
    plt.plot(reward_per_episode)
    plt.show()

    policy = {}
    V = {}
    for s in grid.all_states():
        if s in grid.actions:
            a, max_q = max_dict(Q[s])
            policy[s] = a
            V[s] = max_q

    print("values:")
    print_values(V, grid)

    print("final policy:")
    print_policy(policy, grid)

    print("update counts:")
    total = np.sum(list(update_counts.values()))
    for k, v in update_counts.items():
        update_counts[k] = float(v) / total
    print_values(update_counts, grid)




    
 


    
